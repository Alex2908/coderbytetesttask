<?php

/**
 * Class Travel
 */
class Travel
{
    const TRAVEL_API_PATH = 'travels';

    /**
     * @var string
     */
    private string $id;

    /**
     * @var string
     */
    private string $createdAt;

    /**
     * @var string
     */
    private string $employeeName;

    /**
     * @var string
     */
    private string $departure;

    /**
     * @var string
     */
    private string $destination;

    /**
     * @var string
     */
    private string $price;

    /**
     * @var string
     */
    private string $companyId;

    /**
     * @param string $id
     */
    public function setId ( string $id )
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId (): string
    {
        return $this->id;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt ( string $createdAt )
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt (): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $employeeName
     */
    public function setEmployeeName ( string $employeeName )
    {
        $this->employeeName = $employeeName;
    }

    /**
     * @return string
     */
    public function getEmployeeName (): string
    {
        return $this->employeeName;
    }

    /**
     * @param string $departure
     */
    public function setDeparture ( string $departure )
    {
        $this->departure = $departure;
    }

    /**
     * @return string
     */
    public function getDeparture (): string
    {
        return $this->departure;
    }

    /**
     * @param string $destination
     */
    public function setDestination ( string $destination )
    {
        $this->destination = $destination;
    }

    /**
     * @return string
     */
    public function getDestination (): string
    {
        return $this->destination;
    }

    /**
     * @param string $price
     */
    public function setPrice ( string $price )
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getPrice (): string
    {
        return $this->price;
    }

    /**
     * @param string $companyId
     */
    public function setCompanyId ( string $companyId )
    {
        $this->companyId = $companyId;
    }

    /**
     * @return string
     */
    public function getCompanyId (): string
    {
        return $this->companyId;
    }

}

/**
 * Class Company
 */
class Company
{
    const COMPANY_API_PATH = 'companies';

    /**
     * @var string
     */
    private string $id;

    /**
     * @var string
     */
    private string $createdAt;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $parentId;

    /**
     * @param string $id
     */
    public function setId ( string $id )
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId (): string
    {
        return $this->id;
    }

    /**
     * @param string $createdAt
     */
    public function setCreatedAt ( string $createdAt )
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return string
     */
    public function getCreatedAt (): string
    {
        return $this->createdAt;
    }

    /**
     * @param string $name
     */
    public function setName ( string $name )
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName (): string
    {
        return $this->name;
    }

    /**
     * @param string $parentId
     */
    public function setParentId ( string $parentId )
    {
        $this->parentId = $parentId;
    }

    /**
     * @return string
     */
    public function getParentId (): string
    {
        return $this->parentId;
    }
}

/**
 * Class TestScript
 */
class TestScript
{
    const API_URL = 'https://5f27781bf5d27e001612e057.mockapi.io/webprovise/%s';

    public function execute (): void
    {
        $start = microtime( true );

        $companies = $this->getCompanies();
        $travels = $this->getTravels();

        $travels = $this->convertTravels( $travels );
        $companies = $this->convertCompanies( $companies );
        $travelPrices = $this->getTravelPrices( $travels );
        $companiesCosts = $this->getCompaniesCostsWithTravels( $companies, $travelPrices );

        $result = $this->getCompaniesWithChildren( $companiesCosts );

        echo '<pre>';
        print_r( $result );
        echo '</pre>';

        echo 'Total time: ' . ( microtime( true ) - $start );
    }

    /**
     * @param string $url
     * @return array
     */
    private function getApiResponse ( string $url ): array
    {
        $data = @file_get_contents( $url );

        if ( ! $data ) {
            return [];
        }

        return json_decode( $data, true );
    }

    /**
     * @return array
     */
    private function getCompanies (): array
    {
        $url = sprintf( self::API_URL, Company::COMPANY_API_PATH );

        return $this->getApiResponse( $url );
    }

    /**
     * @return array
     */
    private function getTravels (): array
    {
        $url = sprintf( self::API_URL, Travel::TRAVEL_API_PATH );

        return $this->getApiResponse( $url );
    }

    /**
     * @param array $companies
     * @return array
     */
    private function convertCompanies ( array $companies ): array
    {
        return array_map( function ( $item ) {
            $company = new Company();
            $company->setId( $item['id'] );
            $company->setCreatedAt( $item['createdAt'] );
            $company->setName( $item['name'] );
            $company->setParentId( $item['parentId'] );
            return $company;
        }, $companies );
    }

    /**
     * @param array $travels
     * @return array
     */
    private function convertTravels ( array $travels ): array
    {
        return array_map( function ( $item ) {
            $travel = new Travel();
            $travel->setId( $item['id'] );
            $travel->setPrice( $item['price'] );
            $travel->setCreatedAt( $item['createdAt'] );
            $travel->setDeparture( $item['departure'] );
            $travel->setDestination( $item['destination'] );
            $travel->setCompanyId( $item['companyId'] );
            $travel->setEmployeeName( $item['employeeName'] );
            return $travel;
        }, $travels );
    }

    /**
     * @param array $travels
     * @return array
     */
    private function getTravelPrices ( array $travels ): array
    {
        $totalCosts = [];
        foreach ( $travels as $travel ) {
            $totalCosts[$travel->getCompanyId()] += $travel->getPrice();
        }

        return $totalCosts;
    }

    /**
     * @param array $companies
     * @param array $travelPrices
     * @return array
     */
    private function getCompaniesCostsWithTravels ( array $companies, array $travelPrices ): array
    {
        $result = [];

        foreach ( $companies as $company ) {
            $result[] = [
                'id' => $company->getId(),
                'cost' => $travelPrices[$company->getId()],
                'name' => $company->getName(),
                'parentId' => $company->getParentId()
            ];
        }

        return $result;
    }

    /**
     * @param array $companies
     * @param int $parentId
     * @return array
     */
    private function getCompaniesWithChildren ( array &$companies, $parentId = 0 ): array
    {
        $result = [];

        foreach ( $companies as $company ) {
            if ( $company['parentId'] == $parentId ) {
                $company['children'] = [];
                $children = $this->getCompaniesWithChildren( $companies, $company['id'] );
                if ( $children ) {
                    $company['children'] = $children;
                }
                unset( $company['parentId'] );
                $result[] = $company;
            }
        }

        return $result;
    }
}

( new TestScript() )->execute();
